#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdint.h>

#define SAMPLE_RATE 44100
#define BITS_PER_SAMPLE 16
#define NUM_OF_CHANNELS 1
#define TIME_IN_SEC 2
#define SINE_WAVE_FREQ 16.35

#define PI 3.14159265358979323846

typedef struct PCM16_mono_s
{
    int16_t buff;
} PCM_mono;

PCM_mono *allocate_buffer (int32_t FrameCount)
{
    return (PCM_mono *)malloc(sizeof(PCM_mono) * FrameCount);
}

/*Return the number of audio frames sucessfully written*/
size_t  write_PCM_wav_data(FILE*  f,int32_t FrameCount,PCM_mono  *buffer_p)
{
    size_t temp;

    temp = fwrite(buffer_p, sizeof(PCM_mono), FrameCount,f);
    return temp;
}

//Generates the sound using sine wave

int generate_sound(double amplitude,int32_t SampleRate,int32_t  FrameCount, PCM_mono  *buffer_p)
{
    int temp = 0;
    int16_t sum=0;
    double SampleRate_d = (double)SampleRate;
    double SamplePeriod = 1.0/SampleRate_d;

    double Period;
    double phase;

    int32_t k;

    /*Compute the period*/
    Period = 1.0/SINE_WAVE_FREQ;

    for(k = 0, phase = 0.0; k < FrameCount; k++)
    {
        for(int j=1;j<=10;j++)		//taking the limit from 1 to 10.
        {     phase += j*SamplePeriod;
              phase = (phase > Period)? (phase - Period) : phase; 
              
              sum+= (int16_t)(amplitude*1/(j+1))*sin((k*2.0* PI * SINE_WAVE_FREQ/ SAMPLE_RATE)+phase);
        }
        buffer_p[k].buff = sum;
     }
    return temp;
}

int main()
{
    int i,num_of_samples,temp;

    struct wave_meta{   
        char chunkID[4];
        uint32_t chunkSize;
        char format[4]; 
        char subchunk1ID[4]; 
        uint32_t subchunk1Size; 
        uint16_t audioFormat; 
        uint16_t numChannels; 
        uint32_t sampleRate; 
        uint32_t byteRate; 
        uint16_t blockAlign; 
        uint16_t bitsPerSample; 
        char subchunk2ID[4]; 
        uint32_t subchunk2Size; 
    }wav;

    FILE *f; 

    wav.chunkID[0] = 'R';
    wav.chunkID[1] = 'I';
    wav.chunkID[2] = 'F';
    wav.chunkID[3] = 'F';

    wav.format[0] = 'W';
    wav.format[1] = 'A';
    wav.format[2] = 'V';
    wav.format[3] = 'E';

    wav.subchunk1ID[0] = 'f';
    wav.subchunk1ID[1] = 'm';
    wav.subchunk1ID[2] = 't';
    wav.subchunk1ID[3] = ' ';

    wav.subchunk2ID[0] = 'd';
    wav.subchunk2ID[1] = 'a';
    wav.subchunk2ID[2] = 't';
    wav.subchunk2ID[3] = 'a';

    wav.audioFormat = 1;
    wav.numChannels = NUM_OF_CHANNELS;

    wav.sampleRate = SAMPLE_RATE;
    wav.bitsPerSample = BITS_PER_SAMPLE;
    wav.byteRate = SAMPLE_RATE * NUM_OF_CHANNELS * BITS_PER_SAMPLE/8;
    wav.blockAlign = NUM_OF_CHANNELS * BITS_PER_SAMPLE/8;


    wav.subchunk1Size = 16;
    wav.subchunk2Size = wav.blockAlign * SAMPLE_RATE * TIME_IN_SEC;
    wav.chunkSize = 36 + wav.subchunk2Size;

    double amplitude= 100;     //SHRT_MAX is the minimum value for an object of type short int.

    PCM_mono  *buffer_p = NULL;
    int32_t FrameCount = TIME_IN_SEC * SAMPLE_RATE;
    size_t written;

    /*Open the wav file*/
    if ((f = fopen("out.wav", "wb")) == NULL){
        printf("Error! opening file");
        return 1;
    }

     /*Allocate the data buffer*/
    buffer_p = allocate_buffer(FrameCount);

    if(buffer_p == NULL)
    {
        perror("fopen failed in main");
        return  1;       
    }

    
    temp = generate_sound(amplitude,SAMPLE_RATE,FrameCount,buffer_p);

    if(temp != 0)
    {
        fprintf(stderr, "generate_sound failed in main\n");
        return 1;
    }

    fwrite(&wav, sizeof(wav), 1, f);  
    written=write_PCM_wav_data(f, FrameCount, buffer_p);
    if(written < FrameCount)
    {
        perror("write_PCM_wav_data failed in main");
        return 1;
    }

    free(buffer_p);
    fclose(f);
    return 0;
}
