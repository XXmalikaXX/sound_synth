Defined Variables:
	1. Sample Rate: Standard according to Nyquist theorem.
	2. Bits Per Samples/ Bit Depth = 16 
	3. Number of Channels: Current code uses Monaural sound(mono).
	4. Time and Frequncy: these two units are taken randomly and can be changed.

Functions:
Main:
	1. Meta Data for .wav file.
			All the data are assigned as per equations and required memory.
	2. Opening the file
	3. Allocating memory to the buffer using the function allocate_buffer.
	4. Calling functions generate_sound and write_PCM_wav_data
	5. Closes the opened file and frees the allocated memory of the buffer.

allocate_buffer:
	Allocates memory to the buffer as per requirement which is equal to the Frame Count.

write_PCM_wav_data
	Returns the successfully written number of Audio Frames.

generate_sound:
	Generates the sound using Sine Wave. 
	Uses Discrete Fourier Analysis with the limit from 1 to 10.
        
        f(x)= ∑ Amp(n)*sin(2*pi*freq/sample_rate + phase(n))
          
        Here, Amp(n)  = A*(1/n+1)    
             Phase(n) = phase + n*SamplePeriod    
	where, SamplePeriod = 1/SampleRate
